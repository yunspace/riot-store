# Riot Store 

Item Store CRUD PoC using [Serverless Go](https://github.com/yunspace/serverless-golang). The code covered the following region:

![arch](img/arch.png)

The main purpose of this code is to demonstrate a serverless CRUD API exposing the following endpoints:

	POST /items/
    GET /items/{type}/
    GET /items/{type}/{id}
    PUT /items/{type}/{id}
    DELETE /items/{type}/{id}

Where `Type` is the various Item Types such "Champions", "Skins", "Gameplay" etc that are available in the Store

![store items](img/store_items.png)

DynamoDB uses:

 * Type as Hash Partition Key
 * ID as Range Sort Key. ID is defined as a UUID instead of Item Name. This is so that the Item Name can be updated after creation.

## Usage
The code is deployed to us-west-1. Endpoints are available in PostMan collection: 
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/41fcac00473e79dfc4be)

## Setup
The entire project is built using docker, docker-compose and make. It's useful to have `go` installed. 
Other than that no need to install any other dependencies. 

    go get gitlab.com/riot-games/riot-store
    #cd to directory
	make DOTENV=.env.example dotenv

In the newly generated .env fill in your desired `AWS_PROILE` to use. Docker-compose will mount `~/.aws/` when building.
Alternatively fill in both `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`

	make test build deploy

The above will build and deploy all 5 Lambda Functions plus create DynamoDB resource. 

To clean up:

	make remove

## Improvements
Things that can be done better within the code
* Used DynamoDB since it was the easiest and most economical to setup. In reality should probably go for a SQL based db
* Whilst there are integration tests, ideally there should also be some local runnable unit tests using localstack
* Listing Featured items, Bundles and various other functionalities aren't implemented
* More granular filters and indexes to allow more in depth item searching

## Wish List
Things that I think may be useful
* A mobile app that allows players to buy game items on their phone. Is it possible to take a Hybrid approach similar to React Native?
* Custom recommendations based on player purchases
* A globally replicated SQL compliant DB cluster (similar to Azure CosmosDB). Cockroach seems to be a very interesting candidate in this space.

