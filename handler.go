package main

import (
	"github.com/eawsy/aws-lambda-go-event/service/lambda/runtime/event/apigatewayproxyevt"
	"github.com/eawsy/aws-lambda-go-core/service/lambda/runtime"
	"github.com/yunspace/serverless-golang/aws/event/apigateway"
	"gitlab.com/riot-games/riot-store/services"
	"gitlab.com/riot-games/riot-store/config"
	"go.uber.org/zap"
	"gitlab.com/riot-games/riot-store/models"
	"github.com/satori/go.uuid"
	"net/http"
	"fmt"
	"gitlab.com/riot-games/riot-store/errors"
)

var cfg *config.StoreConfig
var itemsService services.ItemService
var logger *zap.Logger

func init() {
	cfg = config.NewStoreConfig()
	logger, _ = zap.NewProduction()
	itemsService = services.NewDynamoItemsService(cfg, logger)
}

/// Create
func CreateItem(evt *apigatewayproxyevt.Event, _ *runtime.Context) (*apigateway.APIGatewayResponse, error) {
	item, err := models.NewItemFromAPIEvent(evt)
	if err != nil {
		return apigateway.NewAPIGatewayResponseWithError(http.StatusBadRequest, err), nil
	}
	if err := itemsService.Create(item); err != nil {
		return handleError(err), nil
	}
	return apigateway.NewAPIGatewayResponseWithBody(http.StatusCreated, item), nil
}

func GetItem(evt *apigatewayproxyevt.Event, _ *runtime.Context) (*apigateway.APIGatewayResponse, error) {
	itemType, id, err := extractIDAndType(evt)
	if err != nil {
		return handleError(err), nil
	}

	item, err := itemsService.Get(itemType, id)
	if err != nil {
		return handleError(err), nil
	}
	return apigateway.NewAPIGatewayResponseWithBody(http.StatusOK, item), nil
}

func ListItems(evt *apigatewayproxyevt.Event, _ *runtime.Context) (*apigateway.APIGatewayResponse, error) {
	itemType := evt.PathParameters["type"]
	items, err := itemsService.List(itemType)
	if err != nil {
		return nil, err
	}
	return apigateway.NewAPIGatewayResponseWithBody(http.StatusOK, items), nil

}

func UpdateItem(evt *apigatewayproxyevt.Event, _ *runtime.Context) (*apigateway.APIGatewayResponse, error) {
	itemType, id, err := extractIDAndType(evt)
	if err != nil {
		return handleError(err), nil
	}

	item, err := models.NewItemFromAPIEvent(evt)
	if err != nil {
		return apigateway.NewAPIGatewayResponseWithError(http.StatusBadRequest, err), nil
	}
	item.ID = id
	item.Type = itemType

	err = itemsService.Update(item)
	if err != nil {
		return handleError(err), nil
	}
	return apigateway.NewAPIGatewayResponseWithBody(http.StatusOK, item), nil
}

func DeleteItem(evt *apigatewayproxyevt.Event, _ *runtime.Context) (*apigateway.APIGatewayResponse, error) {
	itemType, id, err := extractIDAndType(evt)
	if err != nil {
		return handleError(err), nil
	}

	if err := itemsService.Delete(itemType, id); err != nil {
		return handleError(err), nil
	}

	return apigateway.NewAPIGatewayResponse(http.StatusOK), nil
}

func extractIDAndType(evt *apigatewayproxyevt.Event) (itemType string, id uuid.UUID, err error) {
	id = uuid.FromStringOrNil(evt.PathParameters["id"])
	itemType = evt.PathParameters["type"]
	if itemType == "" || id == uuid.Nil {
		err = fmt.Errorf("Validation error: ItemType: %s Id: %s", itemType, id)
	}
	return itemType, id, err
}

func handleError(err error) *apigateway.APIGatewayResponse {
	switch err := err.(type) {
	case *errors.NotFoundError:
		return apigateway.NewAPIGatewayResponseWithError(http.StatusNotFound, err)
	case *errors.ConflictError:
		return apigateway.NewAPIGatewayResponseWithError(http.StatusConflict, err)
	default:
		return apigateway.NewAPIGatewayResponseWithError(http.StatusInternalServerError, err)
	}
}
