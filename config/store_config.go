package config

import (
	"github.com/kelseyhightower/envconfig"
)

type StoreConfig struct {
	ItemsTable        string `envconfig:"ITEMS_TABLE"`
	AWSRegion		  string `envconfig:"AWS_REGION"`
}

func NewStoreConfig() *StoreConfig {
	c := &StoreConfig{}
	envconfig.MustProcess("", c)
	return c
}
