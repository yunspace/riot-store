package services

import (
	"go.uber.org/zap"
	"github.com/guregu/dynamo"
	"gitlab.com/riot-games/riot-store/config"
	"gitlab.com/riot-games/riot-store/models"
	"time"
	"github.com/satori/go.uuid"
	"gitlab.com/riot-games/riot-store/errors"
	"fmt"
)

type ItemService interface {
	Create(item *models.Item) error
	Get(itemType string, id uuid.UUID) (*models.Item, error)
	List(itemType string) ([]*models.Item, error)
	Update(item *models.Item) error
	Delete(itemType string, id uuid.UUID) error
}

const (
	ItemID   = "ID"
	ItemType = "Type"

	TypeChampions   = "champions"
	TypeTest        = "test"
	TypeSkins       = "skins"
	TypeEmotes      = "emotes"
	TypeGameplay    = "gameplay"
	TypeAccessories = "accessories"
	TypeBundles     = "bundles"
)

type DynamoItemsService struct {
	table  *dynamo.Table
	logger *zap.SugaredLogger
}

var _ ItemService = (*DynamoItemsService)(nil)

func NewDynamoItemsService(config *config.StoreConfig, logger *zap.Logger) *DynamoItemsService {
	db := newDB(config, logger)
	itemsTable := db.Table(config.ItemsTable)
	return &DynamoItemsService{&itemsTable, logger.Sugar()}
}

func (s *DynamoItemsService) Create(item *models.Item) error {
	if item, _ := s.Get(item.Type, item.ID); item != nil {
		return errors.NewConflictError(fmt.Sprintf("Item already exists: Type %s, ID %s", item.Type, item.ID))
	}

	now := time.Now()
	item.CreatedAt = now
	item.UpdatedAt = now

	s.logger.Infow("Create Item", "Item", item)
	return s.table.Put(item).Run()
}

func (s *DynamoItemsService) Get(itemType string, id uuid.UUID) (*models.Item, error) {
	item := &models.Item{}
	s.logger.Infow("Get Item", ItemType, itemType, ItemID, id.String())
	if err := s.table.Get(ItemType, itemType).Range(ItemID, dynamo.Equal, id).Consistent(true).One(item); err != nil {
		if err == dynamo.ErrNotFound {
			return nil, errors.NewNotFoundError(err.Error())
		}
		s.logger.Errorw("Get Item error", "error", err.Error())
		return nil, err
	}
	item.CreatedAt = item.CreatedAt.In(time.Local)
	item.UpdatedAt = item.UpdatedAt.In(time.Local)

	return item, nil
}

func (s *DynamoItemsService) List(itemType string) ([]*models.Item, error) {
	items := []*models.Item{}
	s.logger.Infow("List Items", ItemType, itemType)
	err := s.table.Get(ItemType, itemType).Consistent(false).All(&items)
	if err != nil {
		return nil, err
	}

	return items, nil
}

func (s *DynamoItemsService) Update(item *models.Item) error {
	if _, err := s.Get(item.Type, item.ID); err != nil {
		return err
	}
	s.logger.Infof("Updating Item", "Item", item)
	return s.table.Update( ItemType, item.Type).Range(ItemID, item.ID).
		Set("Name", item.Name).
		Set("RiotPoints", item.RiotPoints).
		Set("InfluencePoints", item.InfluencePoints).
		Set("Featured", item.Featured).
		Set("UpdatedAt", time.Now()).
		Run()
}

func (s *DynamoItemsService) Delete(itemType string, id uuid.UUID) error {
	if _, err := s.Get(itemType, id); err != nil {
		return err
	}
	return s.table.Delete(ItemType, itemType).Range(ItemID, id).Run()
}
