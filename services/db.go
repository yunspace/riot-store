package services

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"gitlab.com/riot-games/riot-store/config"
	"github.com/guregu/dynamo"
	"github.com/aws/aws-sdk-go/aws"
	"go.uber.org/zap"
)


func newDB(config *config.StoreConfig, logger *zap.Logger) *dynamo.DB {
	s, err := session.NewSession()
	if err != nil {
		logger.Fatal("failed to connect to database: %s", zap.Error(err))
	}

	awsConfig := aws.NewConfig()
	awsConfig.Region = aws.String(config.AWSRegion)
	return dynamo.New(s, awsConfig)
}
