package services

import (
	"go.uber.org/zap"
	"gitlab.com/riot-games/riot-store/config"
	"gitlab.com/riot-games/riot-store/models"
	"github.com/satori/go.uuid"
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/guregu/dynamo"
	"gitlab.com/riot-games/riot-store/errors"
)

// Integration tests directly into DynamoDB

func TestCreate_returnSuccessWhenNew(t *testing.T) {
	// given
	itemsService := givenItemService()
	id := uuid.NewV4()

	// when
	err := itemsService.Create(givenItem(TypeTest, id))

	// then
	if assert.NoError(t, err) {
		createdItem := &models.Item{}
		err := itemsService.table.
			Get(ItemType, TypeTest).
			Range(ItemID, dynamo.Equal, id).
			Consistent(true).
			One(createdItem)

		assert.NoError(t, err)
		assert.Equal(t, id, createdItem.ID)
	}
	itemsService.table.Delete(ItemType, TypeTest).Range(ItemID, id).Run()
}

func TestCreate_returnConflictWhenExisting(t *testing.T) {
	// given
	itemsService := givenItemService()
	id := uuid.NewV4()
	itemsService.table.Put(givenItem(TypeTest, id)).Run()

	// when
	err := itemsService.Create(givenItem(TypeTest, id))

	// then
	assert.IsType(t, &errors.ConflictError{}, err)
	itemsService.table.Delete(ItemType, TypeTest).Range(ItemID, id).Run()
}

func TestGet_returnsExistingItem(t *testing.T) {
	// given
	itemsService := givenItemService()
	id := uuid.NewV4()
	itemsService.table.Put(givenItem(TypeTest, id)).Run()

	// when
	item, err := itemsService.Get(TypeTest, id)

	// then
	if assert.NoError(t, err) {
		assert.Equal(t, id, item.ID)
	}
	itemsService.table.Delete(ItemType, TypeTest).Range(ItemID, id).Run()
}

func TestList_returnsExistingItems(t *testing.T) {
	// given
	itemsService := givenItemService()
	id1 := uuid.NewV4()
	id2 := uuid.NewV4()
	id3 := uuid.NewV4()
	itemsService.table.Put(givenItem(TypeTest, id1)).Run()
	itemsService.table.Put(givenItem(TypeTest, id2)).Run()
	itemsService.table.Put(givenItem(TypeTest, id3)).Run()

	// when
	items, err := itemsService.List(TypeTest)

	// then
	if assert.NoError(t, err) {
		assert.Len(t, items, 3)
	}

	itemsService.table.Delete(ItemType, TypeTest).Range(ItemID, id1).Run()
	itemsService.table.Delete(ItemType, TypeTest).Range(ItemID, id2).Run()
	itemsService.table.Delete(ItemType, TypeTest).Range(ItemID, id3).Run()
	assert.NoError(t, err)
}

func TestUpdate_returnsPersistsUpdate(t *testing.T) {
	// given
	itemService := givenItemService()
	id := uuid.NewV4()
	item := givenItem(TypeTest, id)
	itemService.table.Put(item).Run()
	updated := item.UpdatedAt

	// when
	item.Featured = true
	err := itemService.Update(item)

	// then
	if assert.NoError(t, err) {
		updatedItem := &models.Item{}
		itemService.table.
			Get(ItemType, TypeTest).
			Range(ItemID, dynamo.Equal, id).
			Consistent(true).
			One(updatedItem)
		assert.NotEqual(t, updated, updatedItem.UpdatedAt)
		assert.True(t, updatedItem.Featured)
	}

	itemService.table.Delete(ItemType, TypeTest).Range(ItemID, id).Run()
}

func TestUpdate_notFoundReturnsError(t *testing.T) {
	// given
	itemService := givenItemService()
	id := uuid.NewV4()
	item := givenItem(TypeTest, id)

	// when
	item.Featured = true
	err := itemService.Update(item)

	// then
	assert.IsType(t, &errors.NotFoundError{}, err)
}

func TestDelete_deletesExistingItem(t *testing.T) {
	// given
	itemService := givenItemService()
	id := uuid.NewV4()
	item := givenItem(TypeTest, id)
	itemService.table.Put(item).Run()

	// when
	err := itemService.Delete(item.Type, item.ID)

	// then
	if assert.NoError(t, err) {
		deletedItem := &models.Item{}
		err := itemService.table.
			Get(ItemType, TypeTest).
			Range(ItemID, dynamo.Equal, id).
			Consistent(true).
			One(deletedItem)

		assert.IsType(t, dynamo.ErrNotFound, err)
	}

	itemService.table.Delete(ItemType, TypeTest).Range(ItemID, id).Run()
}

func TestDelete_notFoundReturnsError(t *testing.T) {
	// given
	itemService := givenItemService()
	id := uuid.NewV4()
	item := givenItem(TypeTest, id)

	// when
	item.Featured = true
	err := itemService.Delete(item.Type, item.ID)

	// then
	assert.IsType(t, &errors.NotFoundError{}, err)
}

func givenItemService() *DynamoItemsService {
	cfg := config.NewStoreConfig()
	logger, _ := zap.NewDevelopment()
	return NewDynamoItemsService(cfg, logger)
}

func givenItem(itemType string, id uuid.UUID) *models.Item {
	return &models.Item{
		ID:              id,
		Type:            itemType,
		Name:            "Garen",
		RiotPoints:      100,
		InfluencePoints: 200,
	}
}
