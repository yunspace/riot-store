package errors

type BaseError struct {
	error string

}

type NotFoundError struct {
	BaseError
}

func NewNotFoundError(err string) *NotFoundError {
	return &NotFoundError{BaseError{err}}
}

func (e NotFoundError) Error() string {
	return e.error
}

type ConflictError struct {
	BaseError
}

func NewConflictError(err string) *ConflictError {
	return &ConflictError{BaseError{err}}
}

func (e ConflictError) Error() string {
	return e.error
}