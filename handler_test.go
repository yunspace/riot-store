package main

import (
	"testing"
	"io/ioutil"
	"github.com/eawsy/aws-lambda-go-event/service/lambda/runtime/event/apigatewayproxyevt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"github.com/stretchr/testify/mock"
	"gitlab.com/riot-games/riot-store/services"
	"gitlab.com/riot-games/riot-store/models"
	"github.com/satori/go.uuid"
	"encoding/json"
	"fmt"
	"gitlab.com/riot-games/riot-store/errors"
)

type MockItemService struct {
	mock.Mock
}

var _ services.ItemService = (*MockItemService)(nil)

func (m *MockItemService) Create(item *models.Item) error {
	args := m.Called(item)
	return args.Error(0)

}
func (m *MockItemService) Get(itemType string, id uuid.UUID) (*models.Item, error) {
	args := m.Called(itemType, id)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*models.Item), args.Error(1)
}

func (m *MockItemService) List(itemType string) ([]*models.Item, error) {
	args := m.Called(itemType)
	return args.Get(0).([]*models.Item), args.Error(1)
}

func (m *MockItemService) Update(item *models.Item) error {
	args := m.Called(item)
	return args.Error(0)
}

func (m *MockItemService) Delete(itemType string, id uuid.UUID) error {
	args := m.Called(itemType, id)
	return args.Error(0)
}

func TestCreateItem_successReturns201(t *testing.T) {
	// given
	itemJSON := string(givenItemJSON())
	item := givenItem()
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "POST",
		Body:       itemJSON,
	}

	mockItemsService := &MockItemService{}
	itemsService = mockItemsService

	// when
	mockItemsService.On("Create", item).Return(nil)
	resp, err := CreateItem(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusCreated, resp.StatusCode)
		assert.JSONEq(t, resp.Body.(string), itemJSON)
	}
}

func TestCreateItem_invalidReturns400(t *testing.T) {
	// given
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "POST",
		Body:       "",
	}

	mockItemsService := &MockItemService{}
	itemsService = mockItemsService

	// when
	resp, err := CreateItem(evt, nil)

	// then
	mockItemsService.AssertNotCalled(t, "Create")
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	}
}

func TestCreateItem_alreadyExistReturns409(t *testing.T) {
	// given
	item := givenItem()
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "POST",
		Body:       string(givenItemJSON()),
	}

	mockItemsService := &MockItemService{}
	itemsService = mockItemsService

	// when
	mockItemsService.On("Create", item).Return(errors.NewConflictError("item already exists"))
	resp, err := CreateItem(evt, nil)

	// then
	mockItemsService.AssertNotCalled(t, "Create")
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusConflict, resp.StatusCode)
	}
}

func TestCreateItem_errorReturns500(t *testing.T) {
	// given
	item := givenItem()
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "POST",
		Body:       string(givenItemJSON()),
	}

	mockItemsService := &MockItemService{}
	itemsService = mockItemsService

	// when
	mockItemsService.On("Create", item).Return(fmt.Errorf("error creating Item"))
	resp, err := CreateItem(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)
	}
}

func TestGetItem_foundReturns200(t *testing.T) {
	// given
	itemJSON := string(givenItemJSON())
	item := givenItem()
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "GET",
		PathParameters: map[string]string{
			"type": services.TypeTest,
			"id":   item.ID.String(),
		},
	}

	mockItemsService := &MockItemService{}
	mockItemsService.On("Get", services.TypeTest, item.ID).Return(item, nil)
	itemsService = mockItemsService

	// when
	resp, err := GetItem(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusOK, resp.StatusCode)
		assert.JSONEq(t, resp.Body.(string), itemJSON)
	}
}

func TestGetItem_notFoundReturns404(t *testing.T) {
	// given
	id := uuid.NewV4()
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "GET",
		PathParameters: map[string]string{
			"type": services.TypeTest,
			"id":   id.String(),
		},
	}
	errMsg := fmt.Sprintf("Item not found: %s", id.String())

	mockItemsService := &MockItemService{}
	mockItemsService.On("Get", services.TypeTest, id).
		Return(nil, errors.NewNotFoundError(errMsg))
	itemsService = mockItemsService

	// when
	resp, err := GetItem(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusNotFound, resp.StatusCode)
		assert.Equal(t, errMsg, resp.Body)
	}
}

func TestListItems_existingListReturn200(t *testing.T) {
	// given
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "GET",
		PathParameters: map[string]string{
			"type": services.TypeTest,
		},
	}
	items := givenItems()

	mockItemsService := &MockItemService{}
	mockItemsService.On("List", services.TypeTest).Return(items, nil)
	itemsService = mockItemsService

	// when
	resp, err := ListItems(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusOK, resp.StatusCode)
		assert.JSONEq(t, string(givenItemsJSON()), resp.Body.(string))
	}
}

func TestListItems_emptyListReturn200(t *testing.T) {
	// given
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "GET",
		PathParameters: map[string]string{
			"type": services.TypeTest,
		},
	}

	mockItemsService := &MockItemService{}
	mockItemsService.On("List", services.TypeTest).Return(make([]*models.Item, 0), nil)
	itemsService = mockItemsService

	// when
	resp, err := ListItems(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusOK, resp.StatusCode)
		assert.JSONEq(t, "[]", resp.Body.(string))
	}
}

func TestUpdateItem_foundReturn200(t *testing.T) {
	// given
	item := givenItem()
	itemJSON := string(givenItemJSON())
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "PUT",
		PathParameters: map[string]string{
			"type": item.Type,
			"id": item.ID.String(),
		},
		Body: itemJSON,
	}

	mockItemsService := &MockItemService{}
	itemsService = mockItemsService

	// when
	mockItemsService.On("Update", item).Return(nil)
	resp, err := UpdateItem(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusOK, resp.StatusCode)
		assert.JSONEq(t, itemJSON, resp.Body.(string))
	}
}

func TestUpdateItem_notFoundReturn404(t *testing.T) {
	// given
	item := givenItem()
	itemJSON := string(givenItemJSON())
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "PUT",
		PathParameters: map[string]string{
			"type": item.Type,
			"id": item.ID.String(),
		},
		Body: itemJSON,
	}

	mockItemsService := &MockItemService{}
	mockItemsService.On("Update", item).Return(errors.NewNotFoundError("Item not found"))
	itemsService = mockItemsService

	// when
	resp, err := UpdateItem(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusNotFound, resp.StatusCode)
		assert.Equal(t, "Item not found", resp.Body)
	}
}

func TestDeleteItem_successReturn200(t *testing.T) {
	// given
	item := givenItem()
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "DELETE",
		PathParameters: map[string]string{
			"type": item.Type,
			"id": item.ID.String(),
		},
	}

	mockItemsService := &MockItemService{}
	itemsService = mockItemsService

	// when
	mockItemsService.On("Delete", item.Type, item.ID).Return(nil)
	resp, err := DeleteItem(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusOK, resp.StatusCode)
		assert.Nil(t, resp.Body)
	}
}

func TestDeleteItem_notFoundReturn404(t *testing.T) {
	// given
	item := givenItem()
	evt := &apigatewayproxyevt.Event{
		HTTPMethod: "DELETE",
		PathParameters: map[string]string{
			"type": item.Type,
			"id": item.ID.String(),
		},
	}

	mockItemsService := &MockItemService{}
	itemsService = mockItemsService

	// when
	mockItemsService.On("Delete", item.Type, item.ID).Return(errors.NewNotFoundError("not found"))
	resp, err := DeleteItem(evt, nil)

	// then
	mockItemsService.AssertExpectations(t)
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusNotFound, resp.StatusCode)
		assert.Equal(t, "not found", resp.Body)
	}
}

func givenItem() *models.Item {
	item := &models.Item{}
	json.Unmarshal(givenItemJSON(), item)
	return item
}

func givenItemJSON() []byte {
	bytes, _ := ioutil.ReadFile("testdata/item.json")
	return bytes
}

func givenItems() []*models.Item {
	items := make([]*models.Item, 0)
	json.Unmarshal(givenItemsJSON(), &items)
	return items
}

func givenItemsJSON() []byte {
	bytes, _ := ioutil.ReadFile("testdata/items.json")
	return bytes
}
