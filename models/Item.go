package models

import (
	"time"
	"github.com/eawsy/aws-lambda-go-event/service/lambda/runtime/event/apigatewayproxyevt"
	"encoding/json"
	"github.com/satori/go.uuid"
)

type Item struct {
	Type            string    `json:"Type"`
	ID              uuid.UUID `json:"ID"`
	Name            string    `json:"Name"`
	RiotPoints      int       `json:"RiotPoints,omitempty"`
	InfluencePoints int       `json:"InfluencePoints,omitempty"`
	Featured        bool      `json:"Featured"`
	CreatedAt       time.Time `json:"CreatedAt,omitempty"`
	UpdatedAt       time.Time `json:"UpdatedAt,omitempty"`
}

func NewItemFromAPIEvent(evt *apigatewayproxyevt.Event) (*Item, error) {
	item := &Item{}
	if err := json.Unmarshal([]byte(evt.Body), item); err != nil {
		return nil, err
	}
	return item, nil
}
